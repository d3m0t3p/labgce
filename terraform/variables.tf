variable "gcp_project_id" {
  description = "The project id linked to your service account key"
  type        = string
  nullable    = false
}

variable "gcp_service_account_key_file_path" {
  description = "The key linked to your project"
  type        = string
  nullable    = false
}

variable "gce_instance_name" {
  description = "the name of the instance it should be labgce if you followed to tutorial correctly"
  type        = string
  nullable    = false
}

variable "gce_instance_user" {
  description = "The name of the user for the the vm"
  type        = string
  nullable    = false
}

variable "gce_ssh_pub_key_file_path" {
  description = "The path to a public key for which you have the private key, this will be used to authenticate you in the vm using ssh -i path/to/key gce_instance_user@ip"
  type        = string
  nullable    = false
}
