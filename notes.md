## Task 1
## Deliverables : 
none asked
## Task 2
### deliverables 
Explain the usage of each provided file and its contents, add comments to the different blocks if needed (we must ensure that you understood what you have done). Fill the missing documentation parts and link to the online documentation.

 - backend.tf  
 This file is used to define the backend used, currently it is defined as local meaning the state is stored on the local filesystem, it locks that state using system APIs, and performs operations locally.  
 Basicly backends define where Terraform's state snapshots are stored. [documentation](https://www.terraform.io/language/settings/backends)
 - main.tf  
 This is the main configuration file, it is used to declare and configure the ressource infrastructure we are going to put in place. For exemple this is where the boot disk is specified, the network interface configured, the region selected etc. [documentation](https://www.terraform.io/language/resources)
 - outputs.tf  
 Output values make information about your infrastructure available on the command line, and can expose information for other Terraform configurations to use. Output values are similar to return values in programming languages.
 [documentation](https://www.terraform.io/language/values/outputs)
    
 - variables.tf
 This is the configuration file where we declare variable, this is usefull because we can easily change our infrastructure without needing  to change to code. [documentation](https://www.terraform.io/language/values/variables#input-variable-documentation)

 - terraform.tfvars 
 This file is used to store the definition of the the variables aka the values of the variables declared in the variables.tf file (they can be declared in a file with another name). [Documentation](https://learn.hashicorp.com/tutorials/terraform/variables?in=terraform/configuration-language#assign-values-with-a-terraform-tfvars-file)

## Task 3
### Deliverable

What happens if the infrastructure is deleted and then recreated with Terraform? What needs to be updated to access the infrastructure again?

The ip changes and we need to change the file ansible/hosts in order to put the new ip. Furthermore if we didn't put the line host_key_checking = false in the ansible/ansible.cfg the fingerprint of the vm changed and we have an error because the fingetrprint doesn't match anymore. 

## Task 4
### Deliverable
 - **ansible/hosts**  
 This is the file where the inventory is stored, an inventory is a list of host / target on which we can execute commands or playbooks, in our case we define a group named webservers with only one host in the group [documentation](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#inventory-basics-formats-hosts-and-groups)

 - **ansible/playbooks/web.yml**  
 This is a playbook file, this is an ordered lists of tasks, saved so i can run those tasks in that order repeatedly. Playbooks can include variables as well as tasks to adapte to the current host. In our case, this playbook is composed of 5 task, namely : install nginx, copy nginx config file (the ansible/playbooks/files/nginx.conf file), enable configuration, copy index.html and restart nginx.
 We run this playbook against the webserver target (composed of only 1 host) [documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html)

 - **ansible/playbooks/files/nginx.conf**  
 This is the nginx configuration file, it defines a server listening on port 80 serving static content from /usr/share/nginx/html  

 - **ansible/playbooks/templates/index.html.j2**  
 This is a template file, templates are special files where variables are replaced by their value during their processing by the templating engine here the .j2 extension refers to the jinja2 templating engine.
 This template is used to generate the index.html, if we look inside the file we can see {{var}} these are variable usage and will be replaced by their value. [documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html)

## Task6 
*Modify the playbook so that the NGINX restart becomes a handler and the tasks that potentially modify its configuration use notify to call the handler when needed.  Copy the modified playbook into your report.*
Because the file index.html is loaded by nginx each time it serves it, we didn't add a notify on the copy index.html task.

    - name: Configure webserver with nginx
    hosts: webservers
    become: True
    tasks:
        - name: install nginx
        apt: name=nginx update_cache=yes 
        #run apt update and apt install nginx 
        - name: copy nginx config file
        copy: src=files/nginx.conf dest=/etc/nginx/sites-available/default
        notify: restart nginx
        - name: enable configuration
        notify: restart nginx
        file: >
            dest=/etc/nginx/sites-enabled/default
            src=/etc/nginx/sites-available/default
            state=link
        - name: copy index.html
        template: src=templates/index.html.j2 dest=/usr/share/nginx/html/index.html mode=0644
    handlers:
        - name: restart nginx
        service: 
            name: nginx 
            state: restarted




## Taks 7
 1. *Return to the output of running the web.yml playbook the first time. There is one additional task that was not in the playbook. Which one? Among the tasks that are in the playbook there is one task that Ansible marked as ok. Which one? Do you have a possible explanation?*  
The task not in the playbook is : TASK [Gathering Facts]
The task in the playbook noted as OK is enable configuration :`TASK [enable configuration] ok: [gce_instance]`
The key difference between 'changed' and 'ok' in Ansible is the internal agreement between all modules on what is 'ok' and what is 'changed'.
When a module think that it's action changed something (e.g. a state of a subject before module execution and a state after are different), it need to report 'changed' to Ansible. If there were no meaningful changes (definition of 'meaningful' is left to the module), than it reports 'ok'. Sometime modules can detect in advance if changes are needed (example: There is no need to create a directory, as it already exists), sometime it can be detected only after execution of the action (f.e. some application reported 'not changed' to the attempted reconfiguration).

2. *Re-run the web.yml playbook a second time. In principle nothing should have changed. Compare Ansible's output with the first run. Which tasks are marked as changed?*  
none are marked as changed because the host already is in the wanted state 

3. *SSH into the managed server. Modify the NGINX configuration file /etc/nginx/sites-available/default, for example by adding a line with a comment. Re-run the playbook. What does Ansible do to the file and what does it show in its output?*  
TASK [copy nginx config file] is executed and the file with the comment is replaced by the configuration without the comment.
Once all the task are done the handler [restart nginx] is executed.

            PLAY [Configure webserver with nginx] **********************************

            TASK [Gathering Facts] **********************************
            ok: [gce_instance]

            TASK [install nginx] **********************************
            ok: [gce_instance]

            TASK [copy nginx config file] **********************************
            changed: [gce_instance]

            TASK [enable configuration] **********************************
            ok: [gce_instance]

            TASK [copy index.html] **********************************
            ok: [gce_instance]

            RUNNING HANDLER [restart nginx] **********************************
            changed: [gce_instance]

            PLAY RECAP **********************************
            gce_instance               : ok=6    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0


4. *Do something more drastic like completely removing the homepage and repeat the previous question.*
 Compared to the question 3, TASK [copy index.html] is now marked as changed.
  here is the complet rerun 

## Task 7

 